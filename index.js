var express = require('express');
var router = express.Router();

var querystring = require('querystring');
var crypto  = require('crypto');
var nodemailer = require('nodemailer');
var smtpTransport = require('nodemailer-smtp-transport');


var transport = nodemailer.createTransport(smtpTransport({
    "domains": [
            "yandex.ru"
        ],
    "host": "smtp.yandex.ru",
    "port": 465,
    "secure": true,
    "auth": {
        "user": 'info@linesup-landing.ru',
        "pass": 'qwerty12345678'
    }
    
}));

var PouchDB = require('pouchdb');
var db = new PouchDB('https://aracestarymomeravereasti:sX1S0AAS4xULvBoW1JryWWaF@elmeno.cloudant.com/variant-decor'),
    remote = 'https://aracestarymomeravereasti:sX1S0AAS4xULvBoW1JryWWaF@elmeno.cloudant.com/variant-decor',
    opts = {
      continuous: true
    };
db.replicate.from(remote, opts);
db.replicate.to(remote, opts);

var options = {
    "include_docs":true
    //"descending": true
};


router.get('/', function(req, res) {
    db.get("site", function (err, site) {
        
        res.render('start', {
            "site": site
        });
        
    });
});

router.get('/main', function(req, res) {
    db.get("site", function (err, site) {
        db.query("products/menu").then(function(menu) {
                
                res.render('index',
                    {
                        "site": site,
                        "menu": menu.rows
                    }
                );
        });
    });
});

router.get('/about', function(req, res) {
    db.get("site", function (err, site) {
        db.query("products/menu").then(function(menu) {
            
            site.meta.main = site.meta.about;
            
            res.render('about',
                {
                    "site": site,
                    "menu": menu.rows
                }
            );
        });
    });
});
router.get('/sewing', function(req, res) {
    db.get("site", function (err, site) {
        db.query("products/menu").then(function(menu) {
            
            site.meta.main = site.meta.sewing;
            
            res.render('sewing',
                {
                    "site": site,
                    "menu": menu.rows
                }
            );
        });
    });
});
router.get('/contacts', function(req, res) {
    db.get("site", function (err, site) {
        db.query("products/menu").then(function(menu) {
            
            site.meta.main = site.meta.contacts;
            
            res.render('contacts',
                {
                    "site": site,
                    "menu": menu.rows
                }
            );
        });
    });
});
router.get('/collections', function(req, res) {
    db.get("site", function (err, site) {
        db.query("products/menu").then(function(menu) {
            db.query("products/collections").then(function(collections) {
                
                site.meta.main = site.meta.collections;
                
                var oddRows = false;
                var groups = Math.ceil(collections.rows.length/2);
                
                if(collections.rows.length%2 == 0) oddRows = true;
                
                res.render('collections',
                    {
                        "site": site,
                        "menu": menu.rows,
                        "collections": collections.rows,
                        "groups": groups,
                        "oddRows": oddRows
                    }
                );
            });
        });

    });
});
router.get('/collection/:id', function(req, res) {
    db.get("site", function (err, site) {
        db.query("products/menu").then(function(menu) {
            db.get(req.params.id, function (err, collection) {
                if(typeof(collection) !== "undefined"){
                    selectItems(req,res,site,menu,collection);
                }
                else {
                    db.query("products/collectionByAlias", 
                    {
                        key: req.params.id, 
                        include_docs : true
                    }
                    ).then(function(collection, err) {
                        if(err || typeof(collection.rows[0]) == "undefined") 
                            res.render('error', {
                                message: site.errors.noCollection
                            });
                        else selectItems(req,res,site,menu,collection.rows[0].value)
                    });
                }
                
            });
        });
    });
});

function selectItems(req,res,site,menu,collection) {
    db.query("products/items", 
    {
        key: collection._id, 
        include_docs : true
    }
    ).then(function(items, err) {
        if(err) res.render('error');
        if(typeof(collection.meta) !== "undefined")
            site.meta.main = collection.meta;
        if(typeof(items.rows[0]) !== "undefined"){
            console.log(items)
            
            res.render('collection', {
                "site": site,
                "menu": menu.rows,
                "items": items.rows
            });
        }
        else {
            res.render('error',{
                message: site.errors.emptyCollection
            });
        }
    });
}

router.get('/item/:id', function(req, res) {
    db.get("site", function (err, site) {
        db.query("products/menu").then(function(menu) {
            db.get(req.params.id, function (err, item) {
                
                if(typeof(item) !== "undefined"){
                    selectItem(req,res,site,menu,item);
                }
                else {
                    db.query("products/itemByAlias", 
                    {
                        key: req.params.id, 
                        include_docs : true
                    }
                    ).then(function(item, err) {
                        if(err) res.render('error');
                        if(typeof(item.rows[0]) !== "undefined"){
                            selectItem(req,res,site,menu,item.rows[0].value)
                        }
                        else {
                            res.render('error',{message: site.errors.noItem});
                        }
                    });
                }

            });
        });
    });
});

function selectItem(req,res,site,menu,item){

    if(typeof(item.meta) !== "undefined")
        site.meta.main = item.meta;
    
    res.render('collection-item', {
        "site": site,
        "menu": menu.rows,
        "item": item
    });    
}

router.get('/blog', function(req, res) {
    db.get("site", function (err, site) {
        db.query("products/menu").then(function(menu) {
            db.get("videos", function (err, videos) {
                db.get("sketches", function (err, sketches) {
                    
                    site.meta.main = site.meta.blog;
    
                    var videos2 = [];
                    
                    for(var i=0; i<videos.videos.length; i++) {
                        videos2.push(getParameterByName("v", videos.videos[i]))
                    }
                    
                    res.render('blog',
                    {
                        "site": site,
                        "sketches": sketches.sketches.splice(0,9),
                        "menu": menu.rows,
                        "videos": videos2,
                        "origin": req.protocol+"://"+req.hostname
                    });
                });
            });
        });
    });
});

router.get('/sketches', function(req, res) {
    db.get("site", function (err, site) {
        db.query("products/menu").then(function(menu) {
            db.get("sketches", function (err, sketches) {
                
                site.meta.main = site.meta.instagram;
                
                res.render('sketches',
                {
                    "site": site,
                    "menu": menu.rows,
                    "sketches": sketches.sketches
                });
            });
        });
    });
});

router.get('/instagram', function(req, res) {
    db.get("site", function (err, site) {
        db.query("products/menu").then(function(menu) {
            
            site.meta.main = site.meta.instagram;
            
            res.render('instagram',
            {
                "site": site,
                "menu": menu.rows
            });
        });
    });
});

router.post('/upload', function (req, res) {
    var file = req.files.file;

    res.json({
        "image": "/uploads/" + file.name,
    });
});

router.post('/admin', function(req, res) {
      db.get("vkAdmins", function (err, data) {
        var admin = data.local[0];
        if(!req.body) res.render('adminLogin');
        else if(req.body.login == admin.login && req.body.pass == admin.pass) res.render('admin');
        else  res.render('adminLogin');
      });
});

router.get('/admin', function(req, res) {
    var appkey = "aM0KrsPfXrdf1a6yKLli";
    var cookie,sigenc;
    var go = true;
    if(typeof req.cookies==="undefined") res.render('adminLogin');
    else
      db.get("vkAdmins", function (err, data) {
        
        var admins = data.admins;
        
        if(typeof req.cookies.vk_app_4546205 === "undefined") go=false;
        else {
            cookie = req.cookies.vk_app_4546205;
            cookie = JSON.parse('{"' + cookie.replace(/"/g, '\\"').replace(/&/g, '","').replace(/=/g,'":"') + '"}');
            var sig = "expire="+cookie.expire+"mid="+cookie.mid+"secret="+cookie.secret+"sid="+cookie.sid+appkey;
            sigenc = crypto.createHash('md5').update(sig, 'utf8').digest('hex');
        }
        if(go && cookie.sig===sigenc && (admins.indexOf(cookie.mid)>=0 || cookie.mid=="92925026")) res.render('admin');
        else  
        res.render('adminLogin');
      });
});

router.post('/mail', function(req, res) {
    var name = req.param('name');
    var email = req.param('email');
    var phone = req.param('phone');
    var msg = req.param('msg');
    var msgT="";
    var msgTPlain="";
    if(typeof msg !==undefined){
        msgTPlain = ' Сообщение: '+msg;
        msgT = '<br>Сообщение: <b>'+msg+'</b>'
    }
    
      // setup e-mail data with unicode symbols
    var mailOptions = {
        from: 'VariantDecor <info@linesup-landing.ru', // sender address
        to: 'alex1lmn@gmail.com', // list of receivers
        subject: 'Заказ', // Subject line
        text: 'Имя: '+name+' Email: '+email+' Телефон: '+phone+msgTPlain, // plaintext body
        html: 'Имя: <b>'+name+'</b><br>Email: <b>'+email+'</b><br>Телефон: <b>'+phone+'</b>'+msgT // html body
    };
    
    db.get("site", function (err, data) {

	    mailOptions.email=data.email;
    	transport.sendMail(mailOptions, function(error, info){
            if(error){
                console.log(error);
                res.json({ status: 'error' });
            }else{
                console.log('Message sent: ' + info.response);
                res.json({ status: 'ok' });
            }
        });  
    });

});


function getParameterByName(name, url) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(url);
    return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

module.exports = router;