var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var ECT = require('ect');
var cookieSession = require('cookie-session');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
// var Lockit = require('lockit');
var multer = require('multer');
var routes = require('./routes/index');
var qt = require('quickthumb');

var app = express();

var ectRenderer = ECT({ watch: true, root: __dirname + '/views', ext : '.ect' });
app.set('views', __dirname + '/views');
app.set('view engine', 'ect');
app.engine('ect', ectRenderer.render);


app.use(favicon(__dirname + '/public/favicon.ico'));
app.use(logger('dev'));

app.use(cookieSession({
  secret: 'my super secret String'
}));
app.use(cookieParser());

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(multer({dest: __dirname + '/public/uploads'}).single('file'));
// var lockit = new Lockit(config);
// app.use(lockit.router);

app.use(require('less-middleware')(path.join(__dirname, 'public')));
app.use('/', qt.static(__dirname + '/public'));
app.use(express.static(path.join(__dirname, 'public')));



app.use('/', routes);
// lockit(app, config);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error('Страница не найдена!');
    err.status = 404;
    next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function(err, req, res, next) {
        res.status(err.status || 500);
        console.log(err);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});

//exceptions shell not pass!!!
process.on('uncaughtException', function (err) {
  console.log('!Exception: ' + err);
});

module.exports = app;
