var express = require('express');
var router = express.Router();

var querystring = require('querystring');
var crypto = require('crypto');
var nodemailer = require('nodemailer');
var smtpTransport = require('nodemailer-smtp-transport');

var config = require('../config');

var transport = nodemailer.createTransport(smtpTransport(config.mail));

var PouchDB = require('pouchdb');
PouchDB.plugin(require('pouchdb-find'));
var db = new PouchDB('./data');
var remote = new PouchDB(config.db);
var opts = {
  continuous: true,
  live: true,
  retry: true
};
db.replicate.from(remote, opts);
db.replicate.to(remote, opts);

db.sync(remote, opts)
  .on('change', function (change) {
    // yo, something changed!
  }).on('error', function (err) {
    // yo, we got an error! (maybe the user went offline?)
  });

remote.sync(db, opts)
  .on('change', function (change) {
    // yo, something changed!
  }).on('error', function (err) {
    // yo, we got an error! (maybe the user went offline?)
  });

var options = {
  "include_docs": true
  //"descending": true
};

router.get('/', async (req, res) => {
  let site = await db.get("site");

  res.render('start', {
    "site": site
  });

});

router.get('/main', async (req, res) => {
  let site = await db.get("site");
  let menu = await db.find({
    selector: {
      type: 'collection',
      showInMenu: true
    }
  });

  res.render('index', {
    "site": site,
    "menu": menu.docs
  });

});

router.get('/about', async (req, res) => {
  let site = await db.get("site");
  let menu = await db.find({
    selector: {
      type: 'collection',
      showInMenu: true
    }
  });

  site.meta.main = site.meta.about;

  res.render('about', {
    "site": site,
    "menu": menu.docs
  });

});

router.get('/sewing', async (req, res) => {
  let site = await db.get("site");
  let menu = await db.find({
    selector: {
      type: 'collection',
      showInMenu: true
    }
  });

  site.meta.main = site.meta.sewing;

  res.render('sewing', {
    "site": site,
    "menu": menu.docs
  });

});

router.get('/contacts', async (req, res) => {
  let site = await db.get("site");
  let menu = await db.find({
    selector: {
      type: 'collection',
      showInMenu: true
    }
  });

  site.meta.main = site.meta.contacts;

  res.render('contacts', {
    "site": site,
    "menu": menu.docs
  });

});

router.get('/collections', async (req, res) => {
  let site = await db.get("site");
  // let menu = await db.query("products/menu");
  let menu = await db.find({
    selector: {
      type: 'collection',
      showInMenu: true
    }
  });
  let collections = await db.find({
    selector: {
      type: 'collection'
    }
  });


  site.meta.main = site.meta.collections;

  var oddRows = false;
  var groups = Math.ceil(collections.docs.length / 2);

  if (collections.docs.length % 2 == 0) oddRows = true;

  res.render('collections', {
    "site": site,
    "menu": menu.docs,
    "collections": collections.docs,
    "groups": groups,
    "oddRows": oddRows
  });

});


router.get('/collection/:id', async (req, res) => {
  let site = await db.get("site");
  let menu = await db.find({
    selector: {
      type: 'collection',
      showInMenu: true
    }
  });

  try {
    let collection = await db.get(req.params.id);
    selectItems(req, res, site, menu, collection);
  }
  catch(e){

    collection = await db.find({
      selector: {
        type: 'collection',
        alias: req.params.id
      }
    });

    if (typeof (collection.docs[0]) == "undefined"){
      res.render('error', {
        message: site.errors.noCollection
      });
    }
    else selectItems(req, res, site, menu, collection.docs[0])
  }

});

async function selectItems(req, res, site, menu, collection) {
  // let items = await db.query("products/items", {
  //   key: collection._id,
  //   include_docs: true
  // });
  let items = await db.find({
    selector: {
      type: 'item',
      collection: collection._id
    }
  });

  if (typeof (collection.meta) !== "undefined")
    site.meta.main = collection.meta;
  if (typeof (items.docs[0]) !== "undefined") {
    // console.log(items)

    res.render('collection', {
      "site": site,
      "menu": menu.docs,
      "items": items.docs
    });
  } else {
    res.render('error', {
      message: site.errors.emptyCollection
    });
  }
}

router.get('/item/:id', async (req, res) => {
  let site = await db.get("site");
  let menu = await db.find({
    selector: {
      type: 'collection',
      showInMenu: true
    }
  });

  try {
    let item = await db.get(req.params.id);
    selectItem(req, res, site, menu, item);
  }

  catch(e){
    // item = await db.query("products/itemByAlias", {
    //   key: req.params.id,
    //   include_docs: true
    // });

    item = await db.find({
      selector: {
        type: 'item',
        alias: req.params.id
      }
    });

    if (typeof (item.docs[0]) !== "undefined") {
      selectItem(req, res, site, menu, item.docs[0])
    } else {
      res.render('error', {
        message: site.errors.noItem
      });
    }
  }

});

function selectItem(req, res, site, menu, item) {
  if (typeof (item.meta) !== "undefined")
    site.meta.main = item.meta;

  res.render('collection-item', {
    "site": site,
    "menu": menu.docs,
    "item": item
  });
}

router.get('/blog', async (req, res) => {
  let site = await db.get("site");
  let menu = await db.find({
    selector: {
      type: 'collection',
      showInMenu: true
    }
  });
  let videos = await db.get("videos");
  let sketches = await db.get("sketches");

  site.meta.main = site.meta.blog;

  var videos2 = [];

  for (var i = 0; i < videos.videos.length; i++) {
    videos2.push(getParameterByName("v", videos.videos[i]))
  }

  res.render('blog', {
    "site": site,
    "sketches": sketches.sketches.splice(0, 9),
    "menu": menu.docs,
    "videos": videos2,
    "origin": req.protocol + "://" + req.hostname
  });

});

router.get('/sketches', async (req, res) => {
  let site = await db.get("site");
  let menu = await db.find({
    selector: {
      type: 'collection',
      showInMenu: true
    }
  });
  let sketches = await db.get("sketches");

  site.meta.main = site.meta.sketches;

  res.render('sketches', {
    "site": site,
    "menu": menu.docs,
    "sketches": sketches.sketches
  });

});

router.get('/instagram', async (req, res) => {
  let site = await db.get("site");
  let menu = await db.find({
    selector: {
      type: 'collection',
      showInMenu: true
    }
  });

  site.meta.main = site.meta.instagram;

  res.render('instagram', {
    "site": site,
    "menu": menu.docs
  });

});

router.post('/upload', async (req, res) => {
  var file = req.file;

  res.json({
    "image": "/uploads/" + file.filename,
  });
});

router.post('/admin', async (req, res) => {
  let data = await db.get("vkAdmins");
  var admin = data.local[0];
  if (!req.body) res.render('adminLogin');
  else if (req.body.login == admin.login && req.body.pass == admin.pass) res.render('admin', {
    db: config.db,
  });
  else res.render('adminLogin');
});

router.get('/admin', async (req, res) => {
  var appkey = "aM0KrsPfXrdf1a6yKLli";
  var cookie, sigenc;
  var go = true;
  if (typeof req.cookies === "undefined") res.render('adminLogin');
  else {
    let data = await db.get("vkAdmins");

    var admins = data.admins;

    if (typeof req.cookies.vk_app_4546205 === "undefined") go = false;
    else {
      cookie = req.cookies.vk_app_4546205;
      cookie = JSON.parse('{"' + cookie.replace(/"/g, '\\"').replace(/&/g, '","').replace(/=/g, '":"') + '"}');
      var sig = "expire=" + cookie.expire + "mid=" + cookie.mid + "secret=" + cookie.secret + "sid=" + cookie.sid + appkey;
      sigenc = crypto.createHash('md5').update(sig, 'utf8').digest('hex');
    }
    if (go && cookie.sig === sigenc && (admins.indexOf(cookie.mid) >= 0 || cookie.mid == "92925026")) res.render('admin');
    else
      res.render('adminLogin');
  }

});

router.post('/mail', async (req, res) => {
  var name = req.param('name');
  var email = req.param('email');
  var phone = req.param('phone');
  var msg = req.param('msg');
  var msgT = "";
  var msgTPlain = "";
  if (typeof msg !== undefined) {
    msgTPlain = ' Сообщение: ' + msg;
    msgT = '<br>Сообщение: <b>' + msg + '</b>'
  }

  // setup e-mail data with unicode symbols
  var mailOptions = {
    from: 'VariantDecor <info@linesup-landing.ru', // sender address
    to: 'alex1lmn@gmail.com', // list of receivers
    subject: 'Заказ', // Subject line
    text: 'Имя: ' + name + ' Email: ' + email + ' Телефон: ' + phone + msgTPlain, // plaintext body
    html: 'Имя: <b>' + name + '</b><br>Email: <b>' + email + '</b><br>Телефон: <b>' + phone + '</b>' + msgT // html body
  };

  let site = await db.get("site");

  mailOptions.email = site.email;
  mailOptions.to = site.email;
  transport.sendMail(mailOptions, function (error, info) {
    if (error) {
      console.log(error);
      res.json({
        status: 'error'
      });
    } else {
      console.log('Message sent: ' + info.response);
      res.json({
        status: 'ok'
      });
    }
  });

});


function getParameterByName(name, url) {
  name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
  var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
    results = regex.exec(url);
  return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

module.exports = router;