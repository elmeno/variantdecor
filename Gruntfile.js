module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    jshint: {
      files: [
        'assets/js/scripts.js', 
        'Gruntfile.js',
        "app.js"
      ],
      options: {
        browser: true
      }
    },
    concat: {
      options: {
        // define a string to put between each file in the concatenated output
        separator: ';'
      },
      dist: {
        // the files to concatenate
        src: [
          'assets/js/jquery.js',
          // 'assets/js/prefixfree.min.js',
          // 'assets/js/jquery.loadie.min.js',

          // 'assets/js/scripts.js',
          //"public/bootstrap/js/bootstrap.min.js",
          "assets/js/slick.js",
          "assets/js/classie.js",
          "assets/js/instafeed.min.js"
        ],
        // the location of the resulting JS file
        dest: 'public/js/app.js'
      }
    },
    uglify: {
      build: {
        files: {
          'public/js/app.min.js': ['public/js/app.js']
        }
      }
    },
    cssmin: {
      minify: {
        files: {
          'public/css/style.css': [
            'assets/css/style.css',
            'assets/css/media.css',
            "public/bootstrap/css/bootstrap.css",
            "public/css/slick.css",
            'assets/css/animate.css',
            'assets/css/hover.css']  
        }
      }
    }

  });

  // Load plugins
  grunt.loadNpmTasks("grunt-contrib-jshint");
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-cssmin');

  // Default task(s).
  grunt.registerTask('default', [
    'jshint',
    'concat',
    'uglify',
    'cssmin'
  ]);

};