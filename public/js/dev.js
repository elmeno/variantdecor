$(document).ready(function(){
    
    
    
    $("#menuToggle").addClass("animated fadeInLeftBig");
    animationHover($("#menuToggle .text"), 'swing');
    $("#menuToggle").hover(function(){$(this).click()})


    if(page()=="blog") {
        
        //blogSliderInit();
    
        var feed = new Instafeed({
            get: 'user',
            userId: 1582573859,
            accessToken: '1490835221.1677ed0.ba777dfbe9e84e6daf8994d43d143dc4',
            clientId: '8eed5a0bf4de47cdae28ccc165930db2',
            template: '<div class="instPhoto image"><a href="/instagram" target="_self"><img src="{{image}}" /></a></div>',
            limit: 9,
            sortBy:"most-liked",
            resolution: "low_resolution",
            
            after: function(){
    
                var images = $("#instafeed, .sketches>div").find('div');
                $.each(images, function(index, image) {
                  var delay = (index * 75) + 'ms';
                  $(image).css('-webkit-animation-delay', delay);
                  $(image).css('-moz-animation-delay', delay);
                  $(image).css('-ms-animation-delay', delay);
                  $(image).css('-o-animation-delay', delay);
                  $(image).css('animation-delay', delay);
                  $(image).addClass('animated flipInX');
                  
                  setTimeout(function(img){
                        return function(){
                            $(img).removeClass('animated flipInX');
                            $(image).addClass('hvr-grow');
                            $(img).css('-webkit-animation-delay', "0s");
                            $(img).css('-moz-animation-delay', "0s");
                            $(img).css('-ms-animation-delay', "0s");
                            $(img).css('-o-animation-delay', "0s");
                            $(img).css('animation-delay', "0s")
                        }

                  }(image), 1000 + 75*index);
                });
                
                var video = $(".videos");
                $(video).css('animation-delay', images.length*75+1000+"ms");
                $(video).addClass('animated fadeInDownBig');
            }
        });
        
        feed.run();
    }
    
    if(page()=="instagram") {
        var feed = new Instafeed({
            get: 'user',
            userId: 1582573859,
            accessToken: '1490835221.1677ed0.ba777dfbe9e84e6daf8994d43d143dc4',
            clientId: '8eed5a0bf4de47cdae28ccc165930db2',
            template: '<div class="instPhoto image "><a href="{{link}}" target="_blank"><img src="{{image}}" /></a></div>',
            limit: 30,
            sortBy:"most-liked",
            resolution: "low_resolution",
            
            after: function(){
                
                var images = $("#instafeed").find('div');
                $.each(images, function(index, image) {
                  var delay = (index * 75) + 'ms';
                  $(image).css('-webkit-animation-delay', delay);
                  $(image).css('-moz-animation-delay', delay);
                  $(image).css('-ms-animation-delay', delay);
                  $(image).css('-o-animation-delay', delay);
                  $(image).css('animation-delay', delay);
                  $(image).addClass('animated flipInX');
                  
                  setTimeout(function(img){
                        return function(){
                            $(img).removeClass('animated flipInX');
                            $(image).addClass('hvr-grow');
                            $(img).css('-webkit-animation-delay', "0s");
                            $(img).css('-moz-animation-delay', "0s");
                            $(img).css('-ms-animation-delay', "0s");
                            $(img).css('-o-animation-delay', "0s");
                            $(img).css('animation-delay', "0s")
                        }

                  }(image), 1000 + 75*index);
                });
            }
        });
        
        feed.run();  
    }
    
    if(page()=="collections") {

        //collectionsSliderInit();
        
        $(".collections .slider").addClass('animated fadeIn');
        
        paintCollections();
    }
    
    if(page()=="main") {
        animationHover($("#mainLogo"), 'flip');
    }
    if(page()=="sketches") {
        yepnope({
            load: [
                "/fancybox/lib/jquery.mousewheel-3.0.6.pack.js", 
                "/fancybox/source/jquery.fancybox.pack.js?v=2.1.5",
                "/fancybox/source/helpers/jquery.fancybox-buttons.js?v=1.0.5",
                "/fancybox/source/helpers/jquery.fancybox-media.js?v=1.0.6"
            ],
            complete: function () {
                $(".fancybox").fancybox({ padding: 0 });
            }
        });

        

    }

    if(page()=="collection") {
        
        $(".collection .items .item .mainImg img").each(function(index){
            var delay = (index * 75) + 'ms';
            $(this).load(function(item){
                item.css('-webkit-animation-delay', delay);
                item.css('-moz-animation-delay', delay);
                item.css('-ms-animation-delay', delay);
                item.css('-o-animation-delay', delay);
                item.css('animation-delay', delay);
                item.addClass('animated fadeInDownBig');
    
            }($(this).parents(".item")));
        });
        
    }
    
    if(page()=="item") {
        $(function() {
    
          var native_width = 0;
          var native_height = 0;
          var mouse = {x: 0, y: 0};
          var magnify;
          var cur_img;
        
          var ui = {
            magniflier: $('.magniflier')
          };
        
          // Add the magnifying glass
          if (ui.magniflier.length) {
            var div = document.createElement('div');
            div.setAttribute('class', 'glass');
            ui.glass = $(div);
        
            $('body').append(div);
          }
        
          
          // All the magnifying will happen on "mousemove"
        
          var mouseMove = function(e) {
            var $el = $(this);
        
            // Container offset relative to document
            var magnify_offset = cur_img.offset();
        
            // Mouse position relative to container
            // pageX/pageY - container's offsetLeft/offetTop
            mouse.x = e.pageX - magnify_offset.left;
            mouse.y = e.pageY - magnify_offset.top;
            
            // The Magnifying glass should only show up when the mouse is inside
            // It is important to note that attaching mouseout and then hiding
            // the glass wont work cuz mouse will never be out due to the glass
            // being inside the parent and having a higher z-index (positioned above)
            if (
              mouse.x < cur_img.width() &&
              mouse.y < cur_img.height() &&
              mouse.x > 0 &&
              mouse.y > 0
              ) {
        
              magnify(e);
            }
            else {
              ui.glass.fadeOut(100);
            }
        
            return;
          };
        
          var magnify = function(e) {
        
            // The background position of div.glass will be
            // changed according to the position
            // of the mouse over the img.magniflier
            //
            // So we will get the ratio of the pixel
            // under the mouse with respect
            // to the image and use that to position the
            // large image inside the magnifying glass
        
            var rx = Math.round(mouse.x/cur_img.width()*native_width - ui.glass.width()/2)*-1;
            var ry = Math.round(mouse.y/cur_img.height()*native_height - ui.glass.height()/2)*-1;
            var bg_pos = rx + "px " + ry + "px";
            
            // Calculate pos for magnifying glass
            //
            // Easy Logic: Deduct half of width/height
            // from mouse pos.
        
            // var glass_left = mouse.x - ui.glass.width() / 2;
            // var glass_top  = mouse.y - ui.glass.height() / 2;
            var glass_left = e.pageX - ui.glass.width() / 2;
            var glass_top  = e.pageY - ui.glass.height() / 2;
            //console.log(glass_left, glass_top, bg_pos)
            // Now, if you hover on the image, you should
            // see the magnifying glass in action
            ui.glass.css({
              left: glass_left,
              top: glass_top,
              backgroundPosition: bg_pos
            });
        
            return;
          };
        
          $('.magniflier').on('mousemove', function() {
            ui.glass.fadeIn(100);
            
            cur_img = $(this);
        
            var large_img_loaded = cur_img.data('large-img-loaded');
            var src = cur_img.data('large') || cur_img.attr('src');
        
            // Set large-img-loaded to true
            // cur_img.data('large-img-loaded', true)
        
            if (src) {
              ui.glass.css({
                'background-image': 'url(' + src + ')',
                'background-repeat': 'no-repeat'
              });
            }
        
            // When the user hovers on the image, the script will first calculate
            // the native dimensions if they don't exist. Only after the native dimensions
            // are available, the script will show the zoomed version.
            //if(!native_width && !native_height) {
        
              if (!cur_img.data('native_width')) {
                // This will create a new image object with the same image as that in .small
                // We cannot directly get the dimensions from .small because of the 
                // width specified to 200px in the html. To get the actual dimensions we have
                // created this image object.
                var image_object = new Image();
        
                image_object.onload = function() {
                  // This code is wrapped in the .load function which is important.
                  // width and height of the object would return 0 if accessed before 
                  // the image gets loaded.
                  native_width = image_object.width;
                  native_height = image_object.height;
        
                  cur_img.data('native_width', native_width);
                  cur_img.data('native_height', native_height);
        
                  //console.log(native_width, native_height);
        
                  mouseMove.apply(this, arguments);
        
                  ui.glass.on('mousemove', mouseMove);
                };
        
        
                image_object.src = src;
                
                return;
              } else {
        
                native_width = cur_img.data('native_width');
                native_height = cur_img.data('native_height');
              }
            //}
            //console.log(native_width, native_height);
        
            mouseMove.apply(this, arguments);
        
            ui.glass.on('mousemove', mouseMove);
          });
        
          ui.glass.on('mouseout', function() {
            ui.glass.off('mousemove', mouseMove);
          });
        
        });
        
        $(".otherImages .image").click(function(){
            var self = $(this);
            var large   = self.find("img").data("large");
            var normal  = self.find("img").data("normal");
            var preview = self.find("img").data("preview");
            
            var mainImg = $(".mainImg img");
            
            $(".otherImages .image").removeClass("current");
            self.addClass("animated tada");
            mainImg.addClass("animated zoomOutLeft");
            
            setTimeout(function(){
                mainImg.attr("src", normal);
                mainImg.attr("data-large", large);
                
                self.removeClass("animated tada");
                self.addClass("current");
                mainImg.removeClass("animated zoomOutLeft");
                mainImg.addClass("animated bounceInDown");
            }, 1000);
            
        });
        
        $(".otherImages .image").hover(function(){
            $("<img />").attr("src", $(this).data("normal"));
        });
    }
    
    if(page()==""||page()=="start") {
        // $.preloadImages(images);
        
        $(".bf, .startLogo").click(function(){
            $(".bf:first").addClass("active");
            
            setTimeout(function(){
                $(".startPage").fadeOut(500, function(){ 
                    window.location="/main";
                })
            },1000);
        });
    }
    
    currentOrientation();
    
});


var pli=0;
var pliAll;
$.preloadStartImages = function(images) {
  pliAll = images.length;
  for (var i = 0; i < images.length; i++) {
    $("<img onload='pl()' />").attr("src", images[i]);
  }
}
function pl(){
    pli++;

    if(pli == pliAll){
        $(".svg-loader").removeClass("active");
        $(".startPage").addClass("active");
        $.preloadImages(images);
    }
}
var startImg = ["/img/startLogo.png", "/img/bf-body.png", "/img/bf-left-wing.png", "/img/bf-right-wing.png", "/img/start.jpg"];



if(page()=="") $.preloadStartImages(startImg);



$.preloadImages = function(images) {
  for (var i = 0; i < images.length; i++) {
    $("<img />").attr("src", images[i]);
  }
}

var images = ["/img/bg-main-full.jpg", "/img/bg-blog.jpg", "/img/bg-sewing.jpg", "/img/logo-mainPage.png", "/img/logo-menu.png",
"/img/logo-about.png", "/img/instLogo.png", "/img/bg-collections.jpg", "/img/bg-contacts.jpg"];
// -----------------------------------------------------------------------------

function blogSliderInit(){
        if (window.matchMedia("(min-width : 320px) and (max-width : 736px) and (orientation : landscape)").matches) {
            $(".videoSlider").slick(
                {
                  infinite: true,
                  slidesToShow: 2,
                  slidesToScroll: 1,
                  dots: false
                }    
            );      
        }
        else if (window.matchMedia("(max-width: 768px)").matches) {
            $(".videoSlider").slick(
                {
                  infinite: true,
                  slidesToShow: 1,
                  slidesToScroll: 1,
                  dots: false
                }    
            );
        }
        else {
            $(".videoSlider").slick(
                {
                  infinite: true,
                  slidesToShow: 3,
                  slidesToScroll: 1,
                  dots: false
                }    
            );
        }
}

function collectionsSliderInit(){
        
        if (window.matchMedia("(max-width: 480px)").matches) {
            if($(".collections .slide").length%2==1)
                $(".collections .slide:last").addClass("last1");
        }
        else if (window.matchMedia("(min-width : 320px) and (max-width : 736px) and (orientation : landscape)").matches) {
            $(".collections .slider").slick(
                {
                  infinite: true,
                  slidesToShow: 4,
                  slidesToScroll: 1,
                  dots: false,
                  variableWidth: true
                }
            );       
        }
        else if (window.matchMedia("(max-width: 768px)").matches) {
            $(".collections .slider").slick(
                {
                  infinite: true,
                  slidesToShow: 2,
                  slidesToScroll: 1,
                  dots: false,
                  variableWidth: true
                }
            );       
        }
        else {
            $(".collections .slider").slick(
                {
                  infinite: true,
                  slidesToShow: 4,
                  slidesToScroll: 1,
                  dots: false,
                  variableWidth: true
                }
            );
        }
}

function paintCollections(){
    var colors = 
    [
        "158,142,162",
        "144,158,106",
        "106,138,158",
        "185,172,58"
    ];
    
    $(".item.top .overlay").each(function(i){
        $(this).addClass('animated pulse');
        $(this).css({
            "box-shadow": "0 0 1vh 3.5vh rgba("+colors[i%4]+",.5)"
        })
    });
    
    $(".item.bottom .overlay").each(function(i){
        $(this).addClass('animated pulse');
        $(this).css({
            "box-shadow": "0 0 1vh 3.5vh rgba("+colors[3-i%4]+",.5)"
        })
    });
}



    var displayModeLandscape = false;
    var width = 0;
    var height = 0;
        var setPortrait = function() {
        $('html').addClass('portrait').removeClass('landscape');
        displayModeLandscape = false;
    };
    var setLandscape = function() {
        $('html').addClass('landscape').removeClass('portrait');
        displayModeLandscape = true;
    };

    var currentOrientation = function() {
        width = screen.availWidth || $(window).width();
        height = screen.availHeight || $(window).height();
        if(height > width) {
            setPortrait();
        } else {
            setLandscape();
        }
        
        $(".slick-slider").unslick();
        blogSliderInit();
        collectionsSliderInit();
        $(window).on('orientationchange', currentOrientation);
    };
    $(window).on('orientationchange resize', currentOrientation);






function animationHover(element, animation){
    element = $(element);
    element.hover(
        function() {
            element.addClass('animated ' + animation);        
        },
        function(){
            element.one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', 
                function(){
                window.setTimeout( function(){
                    element.removeClass('animated ' + animation);
                }, 0);   
            });
      
        });
}

function animationClick(element, animation){
    element = $(element);
    element.click(
        function() {
            element.addClass('animated ' + animation);        
            //wait for animation to finish before removing classes
            window.setTimeout( function(){
                element.removeClass('animated ' + animation);
            }, 2000);         
  
        });
}



function page(){
    return window.location.pathname.split("/")[1];
}

//----------------- SAY HI --------------------------
console.log(atob("XyAgICBfICAgICAgICBfICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXyAgICAgICAgICAgIF8gICAgICBfICAgICAgICAgICAgICAgICAgICBfICAgXyBfX19fX18gICAgICAgICBfICBfICAgICAgICAgXyAKfCB8ICB8IHwgICAgICB8IHwgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfCB8ICAgICAgICAgIHwgfCAgICAoXykgICAgICAgICAgICAgICAgICB8IHwgfCB8fCBfX18gXCAgICAgICAoXyl8IHwgICAgICAgfCB8CnwgfCAgfCB8ICBfX18gfCB8ICBfX18gIF9fXyAgIF8gX18gX19fICAgIF9fXyAgIHwgfF8gIF9fXyAgICB8IHwgICAgIF8gIF8gX18gICAgX19fICBfX18gfCB8IHwgfHwgfF8vIC8gICBfX18gIF8gfCB8XyAgX19fIHwgfAp8IHwvXHwgfCAvIF8gXHwgfCAvIF9ffC8gXyBcIHwgJ18gYCBfIFwgIC8gXyBcICB8IF9ffC8gXyBcICAgfCB8ICAgIHwgfHwgJ18gXCAgLyBfXCAvIF9ffHwgfCB8IHx8ICBfXy8gICAvIF9ffHwgfHwgX198LyBfIFx8IHwKXCAgL1wgIC98ICBfXy98IHx8IChfX3wgKF8pIHx8IHwgfCB8IHwgfHwgIF9fLyAgfCB8X3wgKF8pIHwgIHwgfF9fX198IHx8IHwgfCB8fCAgX18vXF9fIFx8IHxffCB8fCB8ICAgICAgXF9fIFx8IHx8IHxffCAgX18vfF98CiBcLyAgXC8gIFxfX198fF98IFxfX198XF9fXy8gfF98IHxffCB8X3wgXF9fX3wgICBcX198XF9fXy8gICBcX19fX18vfF98fF98IHxffCBcX19ffHxfX18vIFxfX18vIFxffCAgICAgIHxfX18vfF98IFxfX3xcX19ffChfKQo="));
console.log("Have a good day, developer! Visit our site http://linesup.ru/");

//console.log("SEO-specialists sucks ^_^");