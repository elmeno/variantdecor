var app = angular.module('app', ['ui.bootstrap','dropzone']);

var TabsCtrl = function ($scope)  {
    $scope.tabs = [

        ];
};

var AlertCtrl = function ($scope)  {
    $scope.alerts = [];

    $scope.addAlert = function(alert) {
        $scope.alerts.push(alert);
        $scope.$apply();
    };

    $scope.closeAlert = function(index) {
        $scope.alerts.splice(index, 1);
    };
};

angular.module('dropzone', []).directive('dropzone', function () {
  return function (scope, element, attrs) {
    var config, dropzone;
 
    config = scope[attrs.dropzone];
 
    // create a Dropzone for the element with the given options
    dropzone = new Dropzone(element[0], config.options);
 
    // bind the given event handlers
    angular.forEach(config.eventHandlers, function (handler, event) {
      dropzone.on(event, handler);
    });
  };
});
 

var ProductsCtrl = function ($scope) {
    var self = this;
    self.products = {};
    self.collections = {}; 
    self.newArticle = {
        "type": "item",
        "images" : [
            {
                "image": "http://placehold.it/480x640&text=Best+Size+480x640",
                "preview": "http://placehold.it/150x200&text=Best+Size+150x200"
            }
        ]
    };
    self.currentCollection = "";
    $scope.productImg = {
        'options': {
          'url': '/upload'
        },
        'eventHandlers': {
          'success': function (file, response) {
              self.newArticle.images[self.newArticle.images.length -1].image = response.image;
              self.newArticle.images[self.newArticle.images.length -1].preview = response.image+"?dim=150x200";
              $scope.$apply();
          }
        }
    };
    $scope.productImg2 = {
        'options': {
          'url': '/upload'
        },
        'eventHandlers': {
          'success': function (file, response) {
              self.newArticle.images[self.newArticle.images.length -1].preview = response.image;
              $scope.$apply();
          }
        }
    };

    
    // db.query("products/items").then(function(response) { 
    //     self.products = response.rows;
    //     $scope.$apply();
    // });

    db.find({
        selector: {
          type: 'item'
        }
    }).then(function(response) { 
        self.products = response.docs;
        $scope.$apply();
    });
    

    db.find({
        selector: {
          type: 'collection'
        }
    }).then(function(response) { 
        self.collections = response.docs;
        $scope.$apply();
    });

    // db.query("products/collections").then(function(response) { 
    //     self.collections = response.rows;
    //     $scope.$apply();
    // });
    
    $scope.collectionId = function (product) {
        if (self.currentCollection == product.collection) {
            return true;
        }
        else return false;
    };
    
    
    this.clear = function(){
        self.newArticle = {
            "type": "item",
            "images" : [
                {
                "image": "http://placehold.it/480x640&text=Best+Size+480x640",
                "preview": "http://placehold.it/150x200&text=Best+Size+150x200"
                }
            ]
        };
    } 
    
    this.save = function(product){
        db.put(product, product._rev)
            .then(function(response) { 
                if(response.ok) {
                    product._rev=response.rev;
                }
        });
    }
    this.remove = function(product){
        swal({
            title: "Вы уверены?",
            text: "CTRL+Z не сработает!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Да, удалить!",
            cancelButtonText: "Отменить удаление",

        }, function(isConfirm) {
            if (isConfirm) {
                db.remove(product)
                .then(function(response) { 
                    if(response.ok) {
                        delete product;
                        
                        // db.query("products/items").then(function(response) { 
                        //     self.products = response.rows;
                        //     $scope.$apply();
                        // });
                        db.find({
                            selector: {
                              type: 'item'
                            }
                        }).then(function(response) { 
                            self.products = response.docs;
                            $scope.$apply();
                        });
                        
                    }
    
                });
            }
        });
        
    }

    this.new = function(){
        if(self.newArticle.alias=="")
            self.newArticle.alias = transliterate(self.newArticle.alias);
        
        db.post(self.newArticle)
            .then(function(response) { 
                if(response.ok) {
                    //self.newArticle._rev=response.rev;
                    self.clear();
                    
                    //  db.query("products/items").then(function(response) { 
                    //     self.products = response.rows;
                    //     $scope.$apply();
                    // });
                    db.find({
                        selector: {
                          type: 'item'
                        }
                    }).then(function(response) { 
                        self.products = response.docs;
                        $scope.$apply();
                    });
                    
                }
        });;
    }
    
    this.addPhotos = function(product){
        var newImages = {
                "image": "http://placehold.it/480x640&text=Best+Size+480x640",
                "preview": "http://placehold.it/150x200&text=Best+Size+150x200"
            }
        if(typeof product==="undefined"){
            self.newArticle.images.push(newImages);
            $scope.$apply();
        }
        else {
            product.images.push(newImages);
        }
    }
    
    this.removePhoto = function(index, product){
        if(typeof product==="undefined")
            self.newArticle.images.splice(index+1, 1);
        else {
            product.images.splice(index+1, 1);
        }
    }
    
};

var CollectionsCtrl = function ($scope) {
    var self = this;
    self.products = {}; 
    self.newArticle = {
        "type": "collection",
        "image":"http://placehold.it/300x400&text=Best+Size+300x400",
        "showInMenu": false,
        "desc": []
    };
    self.currentType = "flats";
    $scope.productImg3 = {
        'options': {
          'url': '/upload'
        },
        'eventHandlers': {
          'success': function (file, response) {
              self.newArticle.image = response.image;
              $scope.$apply();
          }
        }
    };
    
    db.find({
        selector: {
          type: 'collection'
        }
    }).then(function(response) { 
        self.products = response.docs;
        $scope.$apply();
    });

    // db.query("products/collections").then(function(response) { 
    //     self.products = response.rows;
    //     $scope.$apply();
    // });
    
    $scope.productType = function (product) {
        if (self.currentType == product.key[0]) {
            return true;
        }
        else return false;
    };
 
    
    this.clear = function(){
        self.newArticle = {
            "type": "collection",
            "image":"http://placehold.it/300x400&text=Best+Size+300x400",
            "showInMenu": false,
            "desc": []
        };
    } 
    
    this.save = function(product){
        db.put(product, product._rev)
            .then(function(response) { 
                if(response.ok) {
                    product._rev=response.rev;
                }
        });
    }
    this.remove = function(product){
        swal({
            title: "Вы уверены?",
            text: "CTRL+Z не сработает!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Да, удалить!",
            cancelButtonText: "Отменить удаление",

        }, function(isConfirm) {
            if (isConfirm) {
                db.remove(product)
                .then(function(response) { 
                    if(response.ok) {
                        delete product;
                        
                        db.find({
                            selector: {
                              type: 'collection'
                            }
                        }).then(function(response) { 
                            self.products = response.docs;
                            $scope.$apply();
                        });
                    }
    
                });
            }
        });
        
    }

    this.new = function(){
        if(self.newArticle.alias=="")
            self.newArticle.alias = transliterate(self.newArticle.alias);
        
        db.post(self.newArticle)
            .then(function(response) { 
                if(response.ok) {
                    //self.newArticle._rev=response.rev;
                    self.clear();
                    
                    db.find({
                        selector: {
                          type: 'collection'
                        }
                    }).then(function(response) { 
                        self.products = response.docs;
                        $scope.$apply();
                    });
                }
        });;
    }
    
    // this.addPhotos = function(product){
    //     var newImages = {
    //             "photo": "http://placehold.it/750x750&text=Best+Size+750x750",
    //             "preview": "http://placehold.it/250x250&text=Best+Size+250x250"
    //         }
    //     if(typeof product==="undefined")
    //         self.newArticle.images.push(newImages);
    //     else {
    //         product.images.push(newImages);
    //     }
    // }
    
    // this.removePhoto = function(index, product){
    //     if(typeof product==="undefined")
    //         self.newArticle.images.splice(index+1, 1);
    //     else {
    //         product.images.splice(index+1, 1);
    //     }
    // }
    
};

var SketchesCtrl = function ($scope) {
    var self = this;
    self.articles = {}; 
    self.newArticle = {
        "image": "",
        "preview": "",
        "title": ""
    };
    $scope.productImg4 = {
        'options': {
          'url': '/upload'
        },
        'eventHandlers': {
          'success': function (file, response) {
              self.newArticle.image = response.image;
              self.newArticle.preview = response.image+"?dim=200x200";
              $scope.$apply();
          }
        }
    };
    $scope.productImg5 = {
        'options': {
          'url': '/upload'
        },
        'eventHandlers': {
          'success': function (file, response) {
              self.newArticle.preview = response.image;
              $scope.$apply();
          }
        }
    };
    db.get("sketches").then(function(response) { 
        self.sketches = response;
        self.articles=self.sketches.sketches;
        $scope.$apply();
    });
    
    
    this.clear = function(){
        self.newArticle = {
        "photo": "",
        "preview": "",
        "title": ""
        };
    } 
    
    this.save = function($scope){
        db.put(self.sketches, self.sketches._rev)
            .then(function(response) { 
                if(response.ok) {
                    self.sketches._rev=response.rev;
                        db.get("sketches").then(function(response) { 
                            self.sketches = response;
                            self.articles=self.sketches.sketches;
                        });
                }
            });;
    }
    this.remove = function(index){
        swal({
            title: "Вы уверены?",
            text: "CTRL+Z не сработает!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Да, удалить!",
            cancelButtonText: "Отменить удаление",

        }, function(isConfirm) {
            if (isConfirm) {
                self.articles.splice(self.articles.length-1-index, 1);
                self.save();
                self.clear();
                $scope.$apply();
            }
        });
    }

    this.new = function(){
        self.articles.push(deepCopy(self.newArticle));
        self.save();
        self.clear();
    }
    
};

var SettingsCtrl = function ($scope) {
    var self = this;

    db.get("site").then(function(response) { 
        self.settings=response;
    });
    
    this.save = function($scope){
        db.put(self.settings, self.settings._rev)
            .then(function(response) { 
                if(response.ok) {
                    self.settings._rev=response.rev;
                    // $scope.addAlert({ type: 'success', msg:"Сохранено!" });
                }
                // else $scope.addAlert({ type: 'error', msg:"Ошибка сохранения!" });

            });;
    }
};

var adminsCtrl = function ($scope) {
    var self = this;

    db.get("vkAdmins").then(function(response) { 
        self.admins=response;
        self.admin = response.admins.join("\n");//self.videoArr.split(/\n/)
        $scope.$apply();
    });
    
    this.save = function($scope){
        self.admins.admins = self.admin.split(/\n/);
        db.put(self.admins, self.admins._rev)
            .then(function(response) { 
                if(response.ok) {
                    self.admins._rev=response.rev;
                }
            });;
    }
};

var videoCtrl = function ($scope) {
    var self = this;

    db.get("videos").then(function(response) { 
        self.videos=response;
        self.video = self.videos.videos.join("\n");

    });
    
    this.save = function($scope){
        self.videos.videos = self.video.split(/\n/);
        console.log(self.videos)
        db.put(self.videos, self.videos._rev)
            .then(function(response) { 
                if(response.ok) {
                    self.videos._rev=response.rev;
                }

            });;
    }
};



app.filter('slice', function() {
  return function(arr, start, end) {
    return (arr || []).slice(start, end);
  };
});

var copyObject = function(obj) {
    var copy = {};
    for (var key in obj) {
        copy[key] = obj[key];
    }
    return copy;
};
var deepCopy = function (obj) {
    if (typeof obj != "object") {
        return obj;
    }
    
    var copy = obj.constructor();
    for (var key in obj) {
        if (typeof obj[key] == "object") {
            copy[key] = this.deepCopy(obj[key]);
        } else {
            copy[key] = obj[key];
        }
    }
    return copy;
};


//Если с английского на русский, то передаём вторым параметром true.
var transliterate = (
	function() {
		var
			rus = "щ   ш  ч  ц  ю  я  ё  ж  ъ  ы  э  а б в г д е з и й к л м н о п р с т у ф х ь".split(/ +/g),
			eng = "shh sh ch cz yu ya yo zh `` y' e` a b v g d e z i j k l m n o p r s t u f x `".split(/ +/g)
		;
		return function(text, engToRus) {
			var x;
			for(x = 0; x < rus.length; x++) {
				text = text.split(engToRus ? eng[x] : rus[x]).join(engToRus ? rus[x] : eng[x]);
				text = text.split(engToRus ? eng[x].toUpperCase() : rus[x].toUpperCase()).join(engToRus ? rus[x].toUpperCase() : eng[x].toUpperCase());	
			}
			return text;
		}
	}
)();
